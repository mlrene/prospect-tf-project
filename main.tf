terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.5.0"
    }
  }
}

provider "aws" {
  region = "ca-central-1"
}


# Configuring a backend to store our state files in s3 and a LockID to ensure only one user can "execute to the dynamodb table at a time"

terraform {
  backend "s3" {
    bucket         = "rene-terraform-backend-s3"
    key            = "terraform.tfstate"
    region         = "ca-central-1"
    dynamodb_table = "rene-terraform-backend-dynamodb"
    encrypt        = false
  }
}

# USING VARIABLES: Variables offer a mechanism to define and reference reusable configuration values in your code. this makes your code flexible

# Method 1 and 2: Variable block with no defined value - Value specified after terraform apply OR specified with terrform apply (terraform apply -var "environment=stage")

# variable "environment" {
#   description = "working environment"
#   type        = string
# }

# variable "instance_type" {
#   description = "working environment"
#   type        = string
# }

# variable "ami" {
#   description = "working environment"
#   type        = string
# }

# Method 3: Variable block with a default value - Default value applied on execution of terraform apply

# variable "environment" {
#   description = "name of the working environment"
#   type = string
#   default = "stage"
# }
# variable "instance_type" {
#   description = "working environment"
#   type        = string
#   default = "t2.micro"
# }

# variable "ami" {
#   description = "working environment"
#   type        = string
#   default = "ami-0e850bfa37f7efe88"
# }

# Method 4: Using a variable definition file with filename ending in .tfvars and then specify that file on the command line with -var-file. (Example: terraform apply -var-file=".tfvars").
# Note: if the variable definition file is named "terraform.tfvars" or ends in ".auto.tfvars", terraform apply command will automatically pick up the appropriate values from the file.

variable "environment" {
  description = "name of the working environment"
  type        = string
}
variable "instance_type" {
  description = "working environment"
  type        = string
}

variable "ami" {
  description = "working environment"
  type        = string
}

# Method 5: Environment Variables - these are used to set secret variable values in the command line so that they are not visible in the variable definition file. (command: export TF_VAR_environment=prod)
# you can also use environment variables to configure you CLI for terrafor using the command "export AWS_ACCESS_KEY_ID= XXXX" & "export AWS_SECRET_KEY=XXXXX"

#RESOURCES
#1: create a vpc in ca-central 

resource "aws_vpc" "vpc1" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "vpc1",
    Env  = var.environment
  }
}


#2: Creating a subnet in vpc1

resource "aws_subnet" "vpc1_pub_subnet1" {
  vpc_id            = aws_vpc.vpc1.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "ca-central-1a"

  tags = {
    Name = "vpc1_pub_subnet1",
    Env  = var.environment
  }
}

#3: Data Sources - referencing the default vpc with a data source block and creating a subnet inside of the referenced "Default VPC

data "aws_vpc" "Default_VPC" {
  default = true

}

resource "aws_subnet" "rene-subnet" {
  vpc_id            = data.aws_vpc.Default_VPC.id
  cidr_block        = "172.31.128.0/20"
  availability_zone = "ca-central-1b"

  tags = {
    Name = "Default_VPC_pub_subnet1"
  }
}

data "aws_vpc" "DMS-VPC" {
  filter {
    name   = "tag:Name"
    values = ["DMS-VPC"]

  }
}

resource "aws_subnet" "DMS-VPC_pub_subnet1" {
  vpc_id            = data.aws_vpc.DMS-VPC.id
  cidr_block        = "10.0.32.0/20"
  availability_zone = "ca-central-1b"

  tags = {
    Name = "DMS-vpc_pub_subnet1"
  }
}

resource "aws_instance" "web" {
  ami           = var.ami
  instance_type = var.instance_type

  tags = {
    Name = "HelloWorld"
  }
}


#4: output block - used to print out configuration data or info of the created resource

output "subnet_id" {
  value = aws_subnet.rene-subnet.id
}

output "arn" {
  value = aws_subnet.rene-subnet.arn
}

output "aws_vpc" {
  value = aws_vpc.vpc1.id
}
output "vpc1_pub_subnet1_cidr_block" {
  value = aws_subnet.vpc1_pub_subnet1.cidr_block
}

output "vpc1_pub_subnet1" {
  value = aws_subnet.vpc1_pub_subnet1.availability_zone_id
}




