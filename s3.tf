
resource "aws_s3_bucket" "jane" {
  bucket = "my-tf-jane-bucket"

  tags = {
    Name        = "jane"
    Environment = "Dev"
  }
}